import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class AddingTest {

    @Test
    void forTwoNumbersShouldReturnSum(){
        Addition adding = new Addition();
        Assertions.assertThat(adding.add(5,5)).isEqualTo(10);
    }
}