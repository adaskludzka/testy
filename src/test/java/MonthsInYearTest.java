import org.assertj.core.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class MonthsInYearTest {

    @ParameterizedTest
    @CsvSource({
            "12, true",
            "123, false",
            "6, false",
    })
    void correctNumbersOfMonthsIs12(int months, boolean result) {
        MonthsInYear monthsInYear = new MonthsInYear();
        Assertions.assertThat(monthsInYear.numberOfMonthsInYear(months)).isEqualTo(result);
    }

}