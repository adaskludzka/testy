import org.assertj.core.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class SecondsInAMinuteTest {

    @ParameterizedTest
    @CsvSource({
            "87, false",
            "60, true",
            "555, false",
    })
    void correctNumbersOfSecondsInAMinuteIs60(int seconds, boolean result) {
        SecondsInAMinute secondsInAMinute = new SecondsInAMinute();
        Assertions.assertThat(secondsInAMinute.numberOfSecondsInAMinute(seconds)).isEqualTo(result);
    }

}