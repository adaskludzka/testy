import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class DivisionTest {

    @Test
    void forTwoNumbersShouldReturnDivision(){
        Division division = new Division();
        Assertions.assertThat(division.divide(5,5)).isEqualTo(1);
    }

}