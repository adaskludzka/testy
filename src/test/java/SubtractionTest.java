import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class SubtractionTest {

    @Test
    void forTwoNumbersShouldReturnSubtraction(){
        Subtraction subtraction = new Subtraction();
        Assertions.assertThat(subtraction.subtract(5,5)).isEqualTo(0);
    }

}