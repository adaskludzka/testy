import org.assertj.core.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class HoursInTheDayTest {

    @ParameterizedTest
    @CsvSource({
            "24, true",
            "125, false",
            "0, false",
    })
    void correctNumbersOfHoursInTheDayIs24(int hours, boolean result) {
        HoursInTheDay hoursInTheDay = new HoursInTheDay();
        Assertions.assertThat(hoursInTheDay.numberOfHoursInTheDay(hours)).isEqualTo(result);
    }

}