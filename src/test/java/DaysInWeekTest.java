import org.assertj.core.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class DaysInWeekTest {

    @ParameterizedTest
    @CsvSource({
            "7, true",
            "500, false",
            "1, false",
    })
    void correctNumbersOfDaysInWeekIs7(int days, boolean result) {
        DaysInWeek daysInWeek = new DaysInWeek();
        Assertions.assertThat(daysInWeek.numberOfDaysInWeek(days)).isEqualTo(result);
    }

}