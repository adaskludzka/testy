import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class GradeTest {

    Grade grade;

    @BeforeEach
    void setUp() {
        grade = new Grade();
    }

    @Test
    void fiftyNineShouldReturnF(){
        Assertions.assertThat(grade.letterOfGrade(59)).isEqualTo('F');
    }
    @Test
    void sixtyEightShouldReturnD(){
        Assertions.assertThat(grade.letterOfGrade(68)).isEqualTo('D');
    }
    @Test
    void seventySevenShouldReturnC(){
        Assertions.assertThat(grade.letterOfGrade(77)).isEqualTo('C');
    }
    @Test
    void eightySixShouldReturnB(){
        Assertions.assertThat(grade.letterOfGrade(86)).isEqualTo('B');
    }
    @Test
    void ninetyFiveShouldReturnA(){
        Assertions.assertThat(grade.letterOfGrade(95)).isEqualTo('A');
    }



}