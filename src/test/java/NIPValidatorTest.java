import org.assertj.core.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class NIPValidatorTest {

    @ParameterizedTest
    @CsvSource({
            "1234567890, true",
            "12345678, false",
            "1234567890112332, false",
            "12345678901123326890, false",
    })
    void correctNIPShouldHave10Characters(String nip, boolean result) {
        NIPValidator nipValidator = new NIPValidator();
        Assertions.assertThat(nipValidator.validateNIP(nip)).isEqualTo(result);
    }


}