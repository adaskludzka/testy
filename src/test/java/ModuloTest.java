import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class ModuloTest {

    @Test
    void forTwoNumbersShouldReturnModuloOperation(){
        Modulo modulo = new Modulo();
        Assertions.assertThat(modulo.moduloOperation(4,5)).isEqualTo(4);
    }

}