import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class PhoneNumberTest {

    PhoneNumber phoneNumber1;

    @BeforeEach
    void setUp() {
        phoneNumber1 = new PhoneNumber();
    }


    @ParameterizedTest
    @CsvSource({
            "+48123456789, true",
            "481234567890, false",

    })
    void isCorrectPhoneNumber(String phoneNumber, boolean result) {
        Assertions.assertThat(phoneNumber1.validatePhoneNumber(phoneNumber)).isEqualTo(result);
    }

    @ParameterizedTest
    @CsvSource({
            "+48123456789, true",
            "+48123456789076, false",

    })
    void forCorrectPhoneNumberShouldReturnLenght12(String phoneNumber, boolean result) {
        Assertions.assertThat(phoneNumber1.checkPhoneNumberLenght(phoneNumber)).isEqualTo(result);
    }


}