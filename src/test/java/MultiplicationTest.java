import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

class MultiplicationTest {
    @Test
    void forTwoNumbersShouldReturnMultiplication(){
        Multiplication multiplication = new Multiplication();
        Assertions.assertThat(multiplication.multiply(5,5)).isEqualTo(25);
    }


}