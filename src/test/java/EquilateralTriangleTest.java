import org.assertj.core.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class EquilateralTriangleTest {

    @ParameterizedTest
    @CsvSource({
            "5,5,6, true",
            "1,2,3, false",
            "100,54,0, false",
    })
    void forEquilateralTriangleTwoSidesMustBeTheSame(int a, int b, int c, boolean result) {
        EquilateralTriangle equilateralTriangle = new EquilateralTriangle();
        Assertions.assertThat(equilateralTriangle.checkIfTriangleIsEquilateral(a,b,c)).isEqualTo(result);
    }

}