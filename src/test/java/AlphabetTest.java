import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class AlphabetTest {

    Alphabet alphabet;

    @BeforeEach
    void setUp() {
        alphabet = new Alphabet();
    }


    @ParameterizedTest
    @CsvSource({
            "ABCDEFGHIJKLMNOPRSTUWXYZ, true",
            "HBCDEFGHIJKLMNOPRSTUWXYZ, false",
    })
    void firstLetterOfAlphabetShouldBeA(String letters, boolean result) {
        Assertions.assertThat(alphabet.checkFirstLetterOfAlphabet(letters)).isEqualTo(result);
    }

    @ParameterizedTest
    @CsvSource({
            "ABCDEFGHIJKLMNOPRSTUWXYZ, true",
            "ABCDEFGHIJKLMNOPRSTU, false",
    })
    void alphabetLengthShouldReturn24(String letters, boolean result) {
        Assertions.assertThat(alphabet.checkIfAlphabetLengthIs24(letters)).isEqualTo(result);
    }



}